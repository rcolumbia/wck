# $Id: /work/modules/tkinter3000/demoMacPython.py 454 2004-09-27T21:40:38.128447Z fredrik  $
# -*- coding: iso-8859-1 -*-
#
# Tkinter 3000
#
# emulate enough of the W widget library to be able to run the second
# demo from J�rg Kantel's article
#
# "How To: Das Modul W oder Grafische Ausgabe in MacPython"
# http://derschockwellenreiter.editthispage.com/python/w_intro.html
#
# for more information on thw W widget library, see:
#
#     "W Widgets"
#     http://www.nevada.edu/~cwebster/Python/WWidgets/index.html
#

import Tkinter
import WCK

root = None

class Widget(WCK.Widget):
    # a W widget

    ui_option_background = "white"

    def __init__(self, bbox, title):
        self.bbox = bbox
        self.init = 0 # not created yet

    def ui_handle_config(self):
        return self.bbox[2] - self.bbox[0], self.bbox[3] - self.bbox[1]

    def ui_handle_repair(self, draw, x0, y0, x1, y1):
        Qd.setdraw(self, draw)
        try:
            self.draw((x0, y0, x1, y1))
        finally:
            Qd.setdraw()

    def ui_handle_destroy(self):
        self.close()

    # W window methods

    def show(self, onoff):
        pass

    def getpossize(self):
        return self.bbox

    def open(self):
        # display widget
        if not self.init:
            self.ui_init(self.master, {})
            self.init = 0
        self.pack()

    def click(self, point, modifiers):
        pass

    def draw(self, region=None):
        pass

    def test(self, xy):
        # check if point is inside widget
        x, y = xy
        w, h = self.ui_size()
        return 0 <= x < w and 0 <= y <= h

    def close(self):
        pass

    def rollover(self, onoff):
        pass

class ClickableWidget(Widget):

    def enable(self, onoff):
        pass

    def callback(self):
        pass

# ... etc ...

class Window(Widget):
    # a toplevel window
    def __init__(self, bbox, title):
        self.bbox = bbox
        self.init = 0
        global root
        if not root:
            master = root = Tkinter.Tk()
        else:
            master = Toplevel(root)
        if title:
            master.title(title)
        self.master = master

# ... etc ...

class QuickDraw:
    # quickdraw emulator (sort of)
    def setdraw(self, widget=None, draw=None):
        self.widget = widget
        self.draw = draw
        if widget:
            self.xy = 0, 0
            self.color = "black"
            self.font = ("Arial", 10)
    # quickdraw operations
    def RGBForeColor(self, rgb):
        self.color = "#%04x%04x%04x" % rgb
    def PaintOval(self, xy):
        self.draw.ellipse(xy, self.widget.ui_brush(self.color))
    def MoveTo(self, x, y):
        self.xy = x, y
    def LineTo(self, x, y):
        xy = x, y
        self.draw.line(self.xy + xy, self.widget.ui_pen(self.color))
        self.xy = xy
    def TextSize(self, size):
        self.font = self.font[0], -size
    def DrawString(self, text):
        font = self.widget.ui_font("black", "{%s} %d" % self.font)
        text = str(text)
        w, h = self.draw.textsize(text, font)
        x, y = self.xy
        self.draw.text((x, y - font.ascent), text, font)
        self.xy = x + w, y

Qd = QuickDraw()

# --------------------------------------------------------------------
# W sample program, from
# http://derschockwellenreiter.editthispage.com/python/w_intro.html

class WB(Window):
    def draw(self, visRgn = None):
        Qd.RGBForeColor( (0, 50000, 50000) )
        Qd.PaintOval( (105, 15, 245, 90) )
        Qd.RGBForeColor( (0, 0, 0) )
        Qd.MoveTo(115, 65)
        Qd.LineTo(228, 65)
        Qd.MoveTo(116, 55)
        Qd.TextSize(20)
        Qd.DrawString("Welcome to")
        Qd.MoveTo(15, 150)
        Qd.TextSize(62)
        Qd.DrawString("MacPython")

dw = WB( (75, 75, 425, 250), "MacPython")
dw.open()

dw.mainloop()

