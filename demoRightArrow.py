#
# Tkinter 3000
#
# this demo shows how to preallocate resources and other data needed
# to redraw the widget
#

from WCK import Widget, FOREGROUND

class RightArrow(Widget):
    # widget implementation

    ui_option_foreground = FOREGROUND

    ui_option_width = 200
    ui_option_height = 200

    def ui_handle_config(self):
        # precalculate brush resource
        self.brush = self.ui_brush(self.ui_option_foreground)
        return int(self.ui_option_width), int(self.ui_option_height)

    def ui_handle_resize(self, width, height):
        # precalculate polygon
        self.arrow = (0, 0, width, height/2, 0, height)

    def ui_handle_repair(self, draw, x0, y0, x1, y1):
        draw.polygon(self.arrow, self.brush)

if __name__ == "__main__":

    import Tkinter

    root = Tkinter.Tk()
    root.title("demoRightArrow")

    widget = RightArrow(root)
    widget.pack(fill="both", expand=1)

    root.mainloop()
