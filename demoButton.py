# $Id: /work/modules/tkinter3000/demoButton.py 176 2004-04-10T10:18:02.938781Z fredrik  $
# Tkinter 3000
#
# this demo shows how to implement a simple button.  the
# first version uses a simple controller, the second the
# standard button controller (via ButtonMixin).
#

from WCK import Widget, TextMixin, ButtonMixin, Controller

class SimpleButtonController(Controller):
    # very simple controller.  see ButtonMixin.ButtonController
    # for a more complete version

    def create(self, handle):
        handle("<Button-1>", self.handle_button_1)
        handle("<ButtonRelease-1>", self.handle_button_release_1)

    def handle_button_1(self, event):
        widget = event.widget
        widget.__relief = widget["relief"]
        widget.config(relief="sunken")

    def handle_button_release_1(self, event):
        widget = event.widget
        widget.config(relief=widget.__relief)
        widget.invoke()

class SimpleButton1(TextMixin, Widget):

    ui_option_text = ""

    ui_option_relief = "raised"
    ui_option_borderwidth = 2

    ui_option_command = None

    ui_controller = SimpleButtonController

    def ui_handle_repair(self, draw, x0, y0, x1, y1):
        if self["relief"] == "sunken":
            draw.settransform((1, 1))
        self.ui_text_draw(draw)

    def ui_text_get(self):
        return str(self.ui_option_text)

    def invoke(self):
        if callable(self.ui_option_command):
            self.ui_option_command()

#
# simple text button, using the button mixin support class

class SimpleButton2(ButtonMixin, TextMixin, Widget):

    ui_option_text = ""

    ui_option_relief = "raised"
    ui_option_borderwidth = 2

    def ui_handle_repair(self, draw, x0, y0, x1, y1):
        if self["relief"] == "sunken":
            draw.settransform((1, 1))
        self.ui_text_draw(draw)

    def ui_text_get(self):
        return str(self.ui_option_text)

    def ui_button_arm(self):
        self.__relief = self["relief"]
        self.config(relief="sunken")

    def ui_button_disarm(self):
        self.config(relief=self.__relief)

#
# simple graphical button

class SimpleButton3(ButtonMixin, Widget):

    ui_option_relief = "raised"
    ui_option_borderwidth = 2

    ui_option_width = 40
    ui_option_height = 40

    def ui_handle_repair(self, draw, x0, y0, x1, y1):
        if self["relief"] == "sunken":
            draw.settransform((1, 1))
        draw.ellipse(
            (x0, y0, x1, y1), self.ui_pen("black"), self.ui_brush("red")
            )

    def ui_handle_config(self):
        return int(self.ui_option_width), int(self.ui_option_height)

    def ui_button_arm(self):
        self.__relief = self["relief"]
        self.config(relief="sunken")

    def ui_button_disarm(self):
        self.config(relief=self.__relief)

if __name__ == "__main__":

    import Tkinter

    root = Tkinter.Tk()
    root.title("demoButton")

    def callback():
        print "Click Click!"

    widget = Tkinter.Button(root, text="Click Me", command=callback,
                            width=20, height=1)
    widget.pack(fill="both", expand=1)

    widget = SimpleButton1(root, text="Click Me", command=callback,
                          width=20, height=2)
    widget.pack(fill="both", expand=1)

    widget = SimpleButton2(root, text="Click Me Too", command=callback,
                           width=20, height=2)
    widget.pack(fill="both", expand=1)

    widget = SimpleButton3(root, command=callback, width=100, height=20)
    widget.pack(fill="both", expand=1)

    root.mainloop()
