# $Id: /work/modules/tkinter3000/setuplib.py 657 2005-04-24T21:05:19.482644Z fredrik  $
# Setup helpers

import os, re, sys

PYVERSION = sys.version[0] + sys.version[2]

class Library:
    def __init__(self):
        self.INCLUDE_DIRS = []
        self.LIBRARY_DIRS = []
        self.LIBRARIES = []

##
# Locate Tcl/Tk libraries.

def find_tk(TCL_ROOT):

    import Tkinter

    lib = Library()

    # override Tcl/Tk library search when registering
    # FIXME: figure out a better way to get the setup action name...
    if "register" in sys.argv:
        return lib

    TCL_VERSION = str(Tkinter.TclVersion)[:3]
    TCLVERSION = TCL_VERSION[0] + TCL_VERSION[2]

    if sys.platform == "win32":
        # windows

        if TCL_ROOT:
            path = [TCL_ROOT]
        else:
            path = [
                os.path.join(sys.prefix, "Tcl"),
                os.path.join("/py" + PYVERSION, "Tcl"),
                os.path.join("/python" + PYVERSION, "Tcl"),
                "/Tcl", "/Tcl" + TCLVERSION, "/Tcl" + TCL_VERSION,
                os.path.join(os.environ.get("ProgramFiles", ""), "Tcl"),
                # FIXME: add more directories here?
                ]
        for root in path:
            TCL_ROOT = os.path.abspath(root)
            if os.path.isfile(os.path.join(TCL_ROOT, "include", "tk.h")):
                break
        else:
            return None

        print "using Tcl/Tk libraries at", TCL_ROOT
        print "using Tcl/Tk version", TCL_VERSION

        version = TCL_VERSION[0] + TCL_VERSION[2]

        lib.INCLUDE_DIRS = [
            os.path.join(TCL_ROOT, "include"),
            ]
        lib.LIBRARY_DIRS = [
            os.path.join(TCL_ROOT, "lib"),
            ]
        lib.LIBRARIES = ["tk" + version, "tcl" + version]

    else:
        # unix: assume libraries are installed in the default location
        lib.INCLUDE_DIRS = []
        lib.LIBRARY_DIRS = []
        lib.LIBRARIES = ["tk" + TCL_VERSION, "tcl" + TCL_VERSION]

    return lib

##
# Extract VERSION tag from file, without importing it.

def find_version(filename):
    for line in open(filename).readlines():
        m = re.search("VERSION\s*=\s*\"([^\"]+)\"", line)
        if m:
            return m.group(1)
    return None

