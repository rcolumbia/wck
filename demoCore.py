# $Id$
# core widgets

import Tkinter

from WCK import coreForm
from WCK import coreButton
from WCK import coreInput

root = Tkinter.Tk()
root.title("WCK Core Widgets")

form = coreForm.Form(root)

def callback():
    print "Click! (%s)" % input.get()

#
# populate the form widget

def X(x):
    return float(x)/4

def Y(y):
    return float(y)/8

def XY(x, y):
    return X(x), Y(y)

form.add(
    XY(7, 7),
    coreButton.Button,
    text="Click Me", command=callback, background="gold"
    )

input = form.add(
    XY(7, 7+14+7),
    coreInput.Input
    )
input.focus_set()

form.add(
    XY(7, 7+14+7+10+7),
    coreButton.Button,
    text="Dismiss", command=root.destroy
    )

form.pack()

root.mainloop()
