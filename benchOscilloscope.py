#
# Line benchmark for Tkinter 3000
#
# Based on original benchmark code by Gordon Williams 99/11/15.
# Simplified and adapted to Tkinter 3000 by Fredrik Lundh, Dec 2000.
#

import time

from math import sin

from Tkinter import *

import WCK

class PlotWidget(WCK.Widget):
    # a simple plotting widget

    ui_option_data = ()

    ui_option_width = 100
    ui_option_height = 100

    ui_option_foreground = "black"

    def ui_handle_config(self):
        self.pen = self.ui_pen(self.ui_option_foreground)
        return int(self.ui_option_width), int(self.ui_option_height)

    def ui_handle_repair(self, draw, x0, y0, x1, y1):
        N = 1000
        print "Drawing", N, "lines"
        t0 = time.clock()
        for i in range(N):
            draw.line(self.ui_option_data, self.pen)
        t1 = time.clock()
        print 'Plotting is ready! Total drawing time:  %f ' % ((t1-t0)/N)

def plotwindow(master, points):

    # FIXME: enable scrollbars
    # scrollbar = Scrollbar(master_scroll, orient=HORIZONTAL)
    can = PlotWidget(master, width=500, height=300, data=points,
                     background="light yellow", foreground="black")
    # scrollbar.config(command=can.xview)
    # scrollbar.pack(side=BOTTOM, fill=X)
    can.pack(side=LEFT, fill=X, expand=1)

# make a bit of data
N = 1000   # number of points
points = []         # here we store the points
for k in range(N):
    points.extend( [k, int(50+50*sin(k/10.))] )

root = Tk()

plotwindow(root, points)

root.mainloop()
