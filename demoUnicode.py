# $Id: /work/modules/tkinter3000/demoUnicode.py 285 2004-06-24T21:20:56.554256Z fredrik  $
# -*- coding: iso-8859-1 -*-
#
# Tkinter 3000
#
# display unicode text
#

from WCK import Widget, TextMixin

class UnicodeLabel(Widget):

    def ui_handle_config(self):
        return 400, 200

    def ui_handle_repair(self, draw, x0, y0, x1, y1):
        font = self.ui_font()
        x = y = 10
        for text in (
             "     ascii: abc",
             "iso-8859-1: ���",
            u"     utf-8: ���".encode("utf-8"),
            u"   unicode: ���"):
            try:
                w, h = draw.textsize(text, font)
                y = y + h
                draw.text((x, y), text, font)
            except (TypeError, ValueError):
                print "*** cannot draw", repr(text)

if __name__ == "__main__":

    import Tkinter

    root = Tkinter.Tk()
    root.title("demoUnicode")

    widget = UnicodeLabel(root)
    widget.pack(fill="both", expand=1)

    root.mainloop()
