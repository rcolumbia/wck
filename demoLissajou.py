#
# Tkinter 3000
#
# a simple double-buffered animation
#

from WCK import Widget, FOREGROUND, FONT

import time, math, sys

if sys.platform == "win32":
    clock = time.clock
else:
    clock = time.time

class Lissajou(Widget):

    ui_option_foreground = FOREGROUND

    ui_option_omega1 = 1
    ui_option_omega2 = 2

    ui_option_width = 200
    ui_option_height = 200

    def __init__(self, master, **options):
        self.phi = 0
        self.dphi = math.pi # per second
        self.start = time.time()
        self.fps = ""
        self.ui_init(master, options)

    def calculate(self, width, height):
        t0 = time.time()
        dt = t0 - self.start
        phi = self.phi
        self.start = t0
        self.phi = self.phi + self.dphi * dt
        # lissajou figure plot (probably stolen from somewhere...)
        omega1 = float(self.ui_option_omega1)
        omega2 = float(self.ui_option_omega2)
        xy = []
        dt = math.pi/100
        pi2 = 2*math.pi + dt
        t = 0
        while t <= pi2:
            xy.append(width/2 + width/3 * math.sin(omega1*t))
            xy.append(height/2 + height/3 * math.cos(omega2*t + phi))
            t = t + dt
        return xy

    def ui_handle_config(self):
        self.pixmap = None
        return int(self.ui_option_width), int(self.ui_option_height)

    def ui_handle_clear(self, draw, x0, y0, x1, y1):
        pass # repair updates entire widget

    def ui_handle_resize(self, width, height):
        self.pixmap = None

    def ui_handle_repair(self, draw, x0, y0, x1, y1):
        t0 = clock()
        width, height = self.ui_size()
        if not self.pixmap:
            self.pixmap = self.ui_pixmap(width, height)
        pen = self.ui_pen(self.ui_option_foreground, 2)
        brush = self.ui_brush(self.ui_option_background)
        self.pixmap.rectangle((0, 0, width, height), brush)
        self.pixmap.polygon(self.calculate(width, height), pen)
        if self.fps:
            font = self.ui_font(self.ui_option_foreground, FONT)
            self.pixmap.text((0, 0), self.fps, font)
        draw.paste(self.pixmap)
	try:
	    self.fps = str(round(1.0 / (clock() - t0), 1)) + " fps"
	except ZeroDivisionError:
	    self.fps = "1000 fps"
        self.after_idle(self.ui_damage) # again!

if __name__ == "__main__":

    import Tkinter

    root = Tkinter.Tk()
    root.title("demoLissajou2 (double buffered)")

    widget = Lissajou(root, background="black", foreground="green")
    widget.pack(fill="both", expand=1)

    root.mainloop()
