#
# Tkinter 3000
#
# using a widget implementation written in C
#

from WCK import Widget

import _tk3demo

import time

class CWidget(Widget):

    def __init__(self, master, **options):
        self.ui_init(master, options)
        self.widget = _tk3demo.widget(self.ui_draw, self.ui_draw.getcapi())

    def ui_handle_resize(self, width, height):
        # delegate to custom object
        self.widget.resize(width, height)

    def ui_handle_repair(self, draw, x0, y0, x1, y1):
        t0 = time.time()
        # delegate to custom object
        self.widget.repair(x0, y0, x1, y1)
        print self.__class__.__name__, "repair in", time.time() - t0, "seconds"

class PythonWidget(Widget):

    def ui_handle_resize(self, width, height):
        self.width = width
        self.height = height

    def ui_handle_repair(self, draw, x0, y0, x1, y1):
        t0 = time.time()
        pen = self.ui_pen("#000000", 0)
        for x in range(0, self.width, 2):
            draw.line((0, 0, x, self.height), pen)
        for x in range(0, self.width, 2):
            draw.line((self.width, self.height, x, 0), pen)
        font = self.ui_font("#ffffff", "helvetica")
        w, h = draw.textsize("python", font)
        draw.text(((self.width - w)/2, 10), "python", font)
        print self.__class__.__name__, "repair in", time.time() - t0, "seconds"

if __name__ == "__main__":

    import Tkinter

    root = Tkinter.Tk()
    root.title("demoC (an extension widget written in C)")

    widget = CWidget(root)
    widget.pack(side="left", fill="both", expand=1)

    widget = PythonWidget(root)
    widget.pack(side="left", fill="both", expand=1)

    root.mainloop()

