#
# Tkinter 3000
#
# this demo shows how to implement a really minimal widget
#

from WCK import Widget

class SimpleRightArrow(Widget):
    # minimal widget implementation

    def ui_handle_repair(self, draw, x0, y0, x1, y1):
        brush = self.ui_brush("black")
        draw.polygon((x0, y0, x1, (y0+y1)/2, x0, y1), brush)

if __name__ == "__main__":

    import Tkinter

    root = Tkinter.Tk()
    root.title("demoSimpleRightArrow")

    widget = SimpleRightArrow(root)
    widget.pack(fill="both", expand=1)
    # widget.manage("pack", fill="both", expand=1)

    root.mainloop()
