#!/usr/bin/env python
#
# Setup script for the Tkinter WCK
# $Id: /work/modules/tkinter3000/setup.py 865 2005-11-11T21:52:07.307427Z Fredrik  $
#
# Usage: python setup.py install
#

import os, re, sys

from glob import glob
from distutils.core import setup, Extension

import setuplib

# make it possible to run the setup script from another directory
try:
    os.chdir(os.path.dirname(sys.argv[0]))
except OSError:
    pass

# --------------------------------------------------------------------
# configuration.  modify as necessary

# location of the Tcl/Tk libraries (use None to search in common locations)
TCL_ROOT = None

# WCK release tag
WCK_VERSION = setuplib.find_version("WCK/__init__.py")

version, date = WCK_VERSION.split("/")
date = date.replace("-", "")

WCK_RELEASE = version + "-" + date

# --------------------------------------------------------------------

tk = setuplib.find_tk(TCL_ROOT)

if not tk:
    print "*** cannot find Tcl/Tk headers and library files"
    print "    change the TCL_ROOT variable in the setup.py file"
    sys.exit(1)

EXTENSIONS = [
    # wck drawing library
    Extension(
        "_tk3draw", ["_tk3draw.c"],
        include_dirs=tk.INCLUDE_DIRS,
        library_dirs=tk.LIBRARY_DIRS,
        libraries=tk.LIBRARIES
        ),
    # wck demo extension (see demoC.py)
    Extension(
        "_tk3demo", ["_tk3demo.c"]
        ),
    ]

if sys.platform == "win32":
    # experimental drawing layer for windows GDI (in progress)
    EXTENSIONS.append(
        Extension(
            "_windraw", ["_windraw.c"],
            libraries=["gdi32", "user32"],
            )
        )

try:
    # add download_url syntax to distutils
    from distutils.dist import DistributionMetadata
    DistributionMetadata.download_url = None
except:
    pass

setup(
    name="tkinter3000",
    version=WCK_RELEASE,
    author="Fredrik Lundh",
    author_email="fredrik@pythonware.com",
    url="http://www.effbot.org/zone/wck.htm",
    download_url="http://www.effbot.org/downloads#tkinter3000",
    description="Widget Construction Kit for Tkinter",
    packages = ["WCK", "tk3"],
    scripts = glob("demo*.py") + glob("bench*.py"),
    ext_modules = EXTENSIONS
    )
