#
# Tkinter 3000
#
# this demo shows how to use the TextMixin for simple text-based
# widgets, and how to use a timer to force the widget to update at
# regular intervals
#

from WCK import Widget, TextMixin

import time

class Clock(TextMixin, Widget):

    ui_option_height = 2
    ui_option_width = 30

    ui_doublebuffer = 1

    def __init__(self, master, **options):
        self.__time = None
        self.ui_init(master, options)
        self.__update()

    def __update(self):
        t = int(time.time())
        if t != self.__time:
            self.ui_damage() # force update
            self.__time = t
        self.after(100, self.__update)

    def ui_handle_repair(self, draw, x0, y0, x1, y1):
        self.ui_text_draw(draw)

    def ui_text_get(self):
        return time.ctime(self.__time)

if __name__ == "__main__":

    import Tkinter

    root = Tkinter.Tk()
    root.title("demoClock")

    widget = Clock(root, font="Times 30", background="gold")
    widget.pack(fill="both", expand=1)

    root.mainloop()
