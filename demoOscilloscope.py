#
# Tkinter 3000
#
# this demo is derived from an original Tk/wxPython benchmark by
# Gordon Williams (see benchOscilloscope.py)
#

from WCK import Widget

import time

class PlotWidget(Widget):
    # a simple plotting widget

    ui_option_width = 100
    ui_option_height = 100

    ui_option_foreground = "green"
    ui_option_background = "black"

    # experimental: set to 1 to enable double buffered display
    # on my box, it's actually *faster* than direct draw !?!?!
    ui_doublebuffer = 0

    def __init__(self, master, **options):
        self.data = []
        self.ui_init(master, options)

    def setdata(self, data):
        self.data = data
        self.ui_damage()

    def ui_handle_config(self):
        self.pen = self.ui_pen(self.ui_option_foreground)
        return int(self.ui_option_width), int(self.ui_option_height)

    def ui_handle_repair(self, draw, x0, y0, x1, y1):
        t = time.clock()
        draw.line(self.data, self.pen)
        t = time.clock() - t
        # print round(1 / t, 2), "fps"

if __name__ == "__main__":

    import Tkinter

    root = Tkinter.Tk()
    root.title("demoOscilloscope")

    import random

    def update():
        # display some new data in the widget.  the widget
        # will update itself as soon as possible
        global offset
        points = []
        x, y = widget.ui_size()
        for k in range(x):
            points.extend([k, random.randint(0, y)])
        widget.setdata(points)
        widget.after(50, update)

    widget = PlotWidget(root, width=500, height=300)
    widget.pack(fill="both", expand=1)

    update()

    root.mainloop()
