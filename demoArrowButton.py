#
# Tkinter 3000
#
# a simple configurable arrow button widget
#

import string

from WCK import ButtonWidget, FOREGROUND

class ArrowButton(ButtonWidget):

    ui_option_foreground = FOREGROUND

    ui_option_width = ui_option_height = 100

    ui_option_direction = "e"

    def ui_handle_config(self):
        self.ui_damage() # just in case direction changed
        return int(self.ui_option_width), int(self.ui_option_height)

    def ui_handle_repair(self, draw, x0, y0, x1, y1):
        if self.cget("relief") == "sunken":
            draw.settransform((1, 1))
        direction = string.lower(self.ui_option_direction)
        brush = self.ui_brush(self.ui_option_foreground)
        if direction == "e":
            draw.polygon((x0, y0, x1, (y0+y1)/2, x0, y1), brush)
        elif direction == "n":
            draw.polygon((x0, y1, (x0+x1)/2, y0, x1, y1), brush)
        elif direction == "w":
            draw.polygon((x1, y0, x0, (y0+y1)/2, x1, y1), brush)
        elif direction == "s":
            draw.polygon((x0, y0, (x0+x1)/2, y1, x1, y0), brush)

if __name__ == "__main__":

    import Tkinter

    root = Tkinter.Tk()
    root.title("demoArrowButton")

    widget = ArrowButton(root, direction="n")
    widget.pack(fill="both", expand=1)

    widget = ArrowButton(root, foreground="red", direction="s")
    widget.pack(fill="both", expand=1)

    widget = ArrowButton(root, foreground="blue", direction="e")
    widget.pack(fill="both", expand=1)

    widget = ArrowButton(root, direction="w")
    widget.pack(fill="both", expand=1)

    root.mainloop()
