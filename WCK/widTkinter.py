#
# The Widget Construction Kit
# $Id: /work/modules/tkinter3000/WCK/widTkinter.py 176 2004-04-10T10:18:02.938781Z fredrik  $
#
# core widgets for Tkinter
#
# history:
# 2004-03-28 fl   created
#
# Copyright (c) 2000-2004 by Secret Labs AB
# Copyright (c) 2000-2004 by Fredrik Lundh
#
# info@pythonware.com
# http://www.pythonware.com
#
# --------------------------------------------------------------------
# The Tkinter 3000 Widget Construction Kit is
#
# Copyright (c) 2000-2004 by Secret Labs AB
# Copyright (c) 2000-2004 by Fredrik Lundh
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and its
# associated documentation for any purpose and without fee is hereby
# granted, provided that the above copyright notice appears in all
# copies, and that both that copyright notice and this permission notice
# appear in supporting documentation, and that the name of Secret Labs
# AB or the author not be used in advertising or publicity pertaining to
# distribution of the software without specific, written prior
# permission.
#
# SECRET LABS AB AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO
# THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
# FITNESS.  IN NO EVENT SHALL SECRET LABS AB OR THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
# OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
# --------------------------------------------------------------------

import Tkinter

##
# Button factory.

def Button(**options):
    return Tkinter.Button, options

##
# Input factory.

class _Input(Entry):
    def set(self, text):
        self.delete(1, Tkinter.END)
        self.insert(1, text)

def Input(**options):
    return _Input, options

# FIXME: to be continued
