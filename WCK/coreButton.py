# $Id: /work/modules/tkinter3000/WCK/coreButton.py 180 2004-04-10T15:19:14.630979Z fredrik  $
# WCK core button implementation
#
# Copyright (c) 2004 by Fredrik Lundh
#

# FIXME: draw dotted focus border
# FIXME: add glyph support

from WCK import *

##
# Core button widget.
#
# @param master The parent widget.
# @param **options Widget configuration options.  The button widget
#     supports all standard options, plus the options listed below.
# @keyparam command Button callback.
# @keyparam font Button font.
# @keyparam foreground Button text colour.
# @keyparam background Button background color.
# @keyparam height Button height, in character units.
# @keyparam width Button width, in character units.

class Button(TextMixin, ButtonWidget):

    __focus = 0

    ui_option_text = ""

    ui_option_height = 1.75 # 14 dialogue units
    ui_option_width = 12.5 # 50 dialogue units

    def ui_handle_repair(self, draw, x0, y0, x1, y1):
        if self.__focus:
            # FIXME: draw dotted line
            draw.rectangle((x0+1, y0+1, x1-2, y1-2), self.ui_pen("black"))
        if self["relief"] == "sunken":
            draw.settransform((1, 1))
        self.ui_text_draw(draw, self.ui_option_text)

    def ui_handle_focus(self, draw, focus):
        self.__focus = focus
        self.ui_damage()
