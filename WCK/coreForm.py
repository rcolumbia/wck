# $Id: coreButton.py 1778 2004-04-10 10:18:02Z fredrik $
# WCK core frame implementation
#
# Copyright (c) 2004 by Fredrik Lundh
#

from WCK import *

##
# Core form widget.
#
# @param master The parent widget.
# @param **options Widget configuration options.  The form widget
#     supports all standard options, plus the options listed below.
# @keyparam font Form default font.
# @keyparam foreground Form text colour.
# @keyparam background Form background color.
# @keyparam height Form height, in character units.
# @keyparam width Form width, in character units.

class Form(Widget):

    ui_option_font = FONT

    ui_option_background = BACKGROUND
    ui_option_foreground = FOREGROUND

    ui_option_height = 188/8
    ui_option_width = 212/4

    def __init__(self, master, **options):
        self.__content = None
        self.ui_init(master, options)

    def ui_handle_config(self):
        self.__font = self.ui_font(
            self.ui_option_foreground, self.ui_option_font
            )
        w, h = self.__charsize = self.__font.measure()
        return (
            int(float(self.ui_option_width)*w),
            int(float(self.ui_option_height)*h)
            )

    def ui_handle_repair(self, draw, x0, y0, x1, y1):
        if self.__content:
            w, h = self.__charsize
            for op, xy, args in self.__content:
                xy = list(xy)
                for i in range(0, len(xy), 2):
                    xy[i] = int(xy[i] * w)
                    xy[i+1] = int(xy[i+1] * h)
                getattr(draw, op)((x, y), *args)

    ##
    # Adds a text label to this form.
    #
    # @param pos Position, in character units.  Note that you can use
    #     fractional values here; a Windows dialogue unit corresponds
    #     to (0.25, 0.125).
    # @param text Text string.
    # @param font Optional font object.  If omitted, the default font
    #     for this form is used instead.

    def draw_text(self, pos, text, font=None):
        if font is None:
            font = self.__font
        self.__content.append(("text", (pos, text, font)))

    ##
    # Adds a child widget to this form.
    #
    # @param xy Widget position, in character units.
    # @param widget_class Child widget.  This can either be a widget
    #     class or a widget instance.
    # @param **options Additional options for the widget class.
    # @keyparam name An optional widget name.
    #

    def add(self, xy, widget_class, **options):
        if isinstance(widget_class, Widget):
            widget = widget_class
            assert widget.master is self, "this widget must be master"
            assert not options, "options cannot be used with instances"
        else:
            widget = widget_class(self, **options)
        w, h = self.__charsize
        try:
            x0, y0, x1, y1 = xy
            # FIXME: take borderwidth into account!
            # FIXME: alternatively
            widget.place(
                x=int(x0*w), y=int(x1*h),
                width=int((x1-x0)*w),
                height=int((y1-y0)*h)
                )
        except ValueError:
            x0, y0 = xy
            widget.place(
                x=int(xy[0]*w), y=int(xy[1]*h)
                )
        return widget

    ##
    # Get value from child widget.

    def get(self, name):
        return self._children[name].get()
