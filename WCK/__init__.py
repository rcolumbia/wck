#
# The Widget Construction Kit
# $Id: /work/modules/tkinter3000/WCK/__init__.py 816 2005-10-16T20:37:26.127213Z Fredrik  $
#
# wck redirector
#

# full version number (used by the setup framework)
VERSION = "1.1/2005-12-11"

# don't export date to Python users
VERSION = VERSION.split("/")[0]

from wckTkinter import *
from Utils import *
