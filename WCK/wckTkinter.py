#
# The Widget Construction Kit
# $Id: /work/modules/tkinter3000/WCK/wckTkinter.py 816 2005-10-16T20:37:26.127213Z Fredrik  $
#
# a pure Python implementation of the uiToolkit widget construction
# kit interface, for Tkinter
#
# history:
# 2000-12-30 fl   created
# 2001-01-03 fl   first preview release
# 2001-01-05 fl   added controller support
# 2001-01-07 fl   added scroll mixin; rename internal mixin methods
# 2001-01-09 fl   experimental support for double-buffered rendering
# 2001-01-10 fl   added border support
# 2001-01-25 fl   minor tweaks; made destroy a bit more robust
# 2001-01-26 fl   added event controller/mixin (for simplied event handling)
# 2001-02-01 fl   changed init handling to better match uiToolkit
# 2002-08-14 fl   tweaks to the pythondoc markup
# 2003-05-09 fl   more pythondoc/docstring tweaks
# 2003-11-15 fl   more pythondoc/docstring tweaks
# 2003-12-06 fl   added ui_path stub (1.0)
# 2004-04-10 fl   added takefocus support
# 2004-09-05 fl   added ui_setcontroller method
# 2004-09-27 fl   driver refactoring (in progress)
# 2005-09-12 fl   added support for (mode, size, data) images
#
# Copyright (c) 2000-2005 by Secret Labs AB
# Copyright (c) 2000-2005 by Fredrik Lundh
#
# info@pythonware.com
# http://www.pythonware.com
#
# --------------------------------------------------------------------
# The Tkinter 3000 Widget Construction Kit is
#
# Copyright (c) 2000-2005 by Secret Labs AB
# Copyright (c) 2000-2005 by Fredrik Lundh
#
# By obtaining, using, and/or copying this software and/or its
# associated documentation, you agree that you have read, understood,
# and will comply with the following terms and conditions:
#
# Permission to use, copy, modify, and distribute this software and its
# associated documentation for any purpose and without fee is hereby
# granted, provided that the above copyright notice appears in all
# copies, and that both that copyright notice and this permission notice
# appear in supporting documentation, and that the name of Secret Labs
# AB or the author not be used in advertising or publicity pertaining to
# distribution of the software without specific, written prior
# permission.
#
# SECRET LABS AB AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO
# THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
# FITNESS.  IN NO EVENT SHALL SECRET LABS AB OR THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
# OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
# --------------------------------------------------------------------

# TODO: finalize bitmap/image support
# TODO: ui_handle_map ???
# TODO: a "wck tour" demo script

##
# The Widget Construction Kit (WCK) is an extension API that allows you
# to implement all sorts of custom widgets, in pure Python.
# <p>
# For more information on the Widget Construction Kit, see
# <a href="http://effbot.org/zone/wck.htm">the WCK page</a> over at
# <a href="http://effbot.org">effbot.org</a>.
#
# <h3>Using the WCK Module</h3>
#
# To implement a new WCK widget, all you have to do is to subclass the
# {@link Widget} class, and implement one or more hook methods.
# <p>
# Here's a very simple example. This widget displays the text "hello,
# world" in the upper left corner:
#
# <pre>
# class HelloWorld(Widget):
#
#     def ui_handle_repair(self, draw, x0, y0, x1, y1):
#         font = self.ui_font("black", "times")
#         draw.text((0, 0), "hello, world", font)
#
# widget = HelloWorld(root)
# widget.pack()
# </pre>
#
# You can override the following <b>Widget</b> hook methods:
# <ul>
# <li>{@link Widget.ui_handle_clear} is called to redraw the
# background.</li>
# <li>{@link Widget.ui_handle_config} is called when the widget is
# (re)configured.</li>
# <li>{@link Widget.ui_handle_damage} is called to indicate that some
# region needs to be redrawn.  The widget should redraw itself in
# <b>ui_handle_repair</b>, not in this method.</li>
# <li>{@link Widget.ui_handle_destroy} is called to release widget
# resources.</li>
# <li>{@link Widget.ui_handle_focus} is called to redraw the focus
# indicator.</li>
# <li>{@link Widget.ui_handle_repair} is called to redraw damaged
# regions.</li>
# <li>{@link Widget.ui_handle_resize} is called when the widget's
# geometry is changed.</li>
# <li>{@link Widget.ui_handle_select} is called to handle cut
# buffers/selections.</li>
# </ul>
# All methods have default implementations, but you probably want to
# override at least <b>ui_handle_repair</b> in your widget.
##

import Tkinter
import sys

# standard drawing engine
import _tk3draw

Error = Tkinter.TclError

if sys.platform == "win32":
    FONT = "{ms sans serif} 8"
    FOREGROUND = "systembuttontext"
    BACKGROUND = "systembuttonface"
    SELECTFOREGROUND = "systemhighlighttext"
    SELECTBACKGROUND = "systemhighlight"
else:
    # FIXME: query Tkinter's option database?
    FONT = "Helvetica 12"
    FOREGROUND = "black"
    BACKGROUND = "grey"
    SELECTFOREGROUND = "black"
    SELECTBACKGROUND = "gold"

# used to hide inherited options
NOT_INHERITED = [None]

##
# Experimental style object.

class Style:
    # FIXME: move this to _tk3draw

    margin = 0

    def __init__(self, widget):
        margin = max(int(widget.ui_option_highlightthickness), 0)
        border = max(int(widget.ui_option_borderwidth), 0)
        relief = widget._ui_driver._lookuprelief(str(widget.ui_option_relief))
        self.background = widget.ui_brush(widget.ui_option_background)
        if margin + border > 0:
            self.margin = margin + border
            self.border = self.background, margin, border, relief

    ##
    # Gets the margin width.
    #
    # @return The margin, in pixels.

    def getmargin(self):
        return self.margin

    ##
    # Draws the widget background.
    #
    # @param xy Background extent.
    # @param draw Drawing context.  This is an object implementing
    #    the {@link DrawInterface} interface.

    def drawbackground(self, xy, draw):
        draw.rectangle(xy, self.background)

    ##
    # Draws the widget border.
    #
    # @param xy Background extent.
    # @param draw Drawing context.  This is an object implementing
    #    the {@link DrawInterface} interface.

    def drawborder(self, xy, draw):
        try:
            if self.margin:
                apply(draw._border, self.border)
        except AttributeError:
            pass

##
# The WCK widget base class.  This class implements a generic WCK
# widget, which should be subclassed to provide drawing and event
# handling behaviour.

class Widget(Tkinter.Widget):

    ##
    # (Class attribute) Background color.

    ui_option_background = BACKGROUND

    ##
    # (Class attribute) Border relief.

    ui_option_relief = "flat"

    ##
    # (Class attribute) Border width.  If not zero, the border is
    # decorated according to the <b>relief</b> setting.

    ui_option_borderwidth = 0

    ##
    # (Class attribute) Focus region width.

    ui_option_highlightthickness = 0

    ##
    # (Class attribute) Cursor to use when the mouse is moved over
    # this widget.  If empty, the default cursor is used.

    ui_option_cursor = ""

    ##
    # (Class attribute) Focus handling.

    ui_option_takefocus = ""

    ##
    # (Class attribute) Control double buffering for this widget.  If
    # set to a true value, the <b>ui_handle_repair</b> method will be
    # set up to draw in an off-screen buffer, which is then copied to
    # the screen in one step.

    ui_doublebuffer = 0

    ##
    # (Class attribute) Standard event controller for this widget.

    ui_controller = None

    ##
    # (Instance attribute) (Experimental) Drawing context for this
    # widget.

    ui_draw = None

    ##
    # Creates a widget instance.
    # <p>
    # This method simply calls the <b>ui_init</b> method.  If you
    # need to initalize instance variables, you can override this
    # method.  You must remember to call <b>ui_init</b> when done
    # (or if you prefer, call Widget.__init__).
    #
    # @param master The parent widget.
    # @param **options Widget configuration options.  You can use
    #    standard keyword options (listed below), or widget-specific
    #    extra options.
    # @keyparam background The background colour.
    # @keyparam relief The border style.
    # @keyparam borderwidth The border width, in pixels.
    # @keyparam highlightthickness The colour to use for a highlighted
    #    border.
    # @keyparam cursor The cursor to use when the mouse pointer is
    #    moved in this widget.

    def __init__(self, master, **options):
        self.ui_init(master, options)

    ##
    # Initializes a widget instance.
    #
    # @param master The parent widget.
    # @param options A dictionary containing configuration options.

    def ui_init(self, master, options=None):
        "Initialize widget instance"

        # create a frame widget
        Tkinter.Widget.__init__(
            self, master, "frame", {
                "background": "",
                "takefocus": self.ui_option_takefocus,
                }
            )

        draw = _tk3draw.getdraw(self.tk.interpaddr(), str(self))

        self.ui_draw = self._ui_driver = draw

        self.__size = 100, 100 # default size
        self.__style = None # default style
        self.__visible = 0
        self.__repair_pending = 0
        self.__controller = None

        # set up resource caches
        try:
            cache = self.__cache
        except AttributeError:
            try:
                self.__cache = master.__cache
            except AttributeError:
                if self.winfo_toplevel() is master:
                    self.__cache = master.__cache = {}
                else:
                    self.__cache = {}
        try:
            cache = self.__fontcache
        except AttributeError:
            self.__fontcache = {}

        # install default controller
        default_controller = getcontroller(_DefaultController, self)
        tags = self.bindtags()
        self.bindtags(tags[:1] + (default_controller.tag,) + tags[2:])

        if self.ui_controller is not None:
            self.ui_setcontroller(self.ui_controller)

        # initial configure
        if not options:
            options = {}
        self.__configure(options, self.__size)

    def _ui_destroy(self):
        # internal
        if hasattr(self, "ui_draw"):
            try:
                _controller_tags[self.__controller].detach(self)
            except KeyError:
                pass
            try:
                self.ui_handle_destroy()
            finally:
                save = self.tk, self.master, self.children
                self.__dict__.clear()
                self.tk, self.master, self.children = save

    ##
    # Destroys a widget instance.

    def destroy(self):
        "Destroy widget"
        try:
            Tkinter.Widget.destroy(self)
        finally:
            self._ui_destroy()

    #
    # option database

    def __configure(self, options, default_size=None):

        items = options.items()

        values = []
        try:
            for option, value in items:
                value = getattr(self, "ui_option_" + option)
                if value is NOT_INHERITED:
                    raise AttributeError
                values.append(value)
        except AttributeError:
            raise Error, "unsupported option: %s" % option

        for option, value in items:
            setattr(self, "ui_option_" + option, value)

        try:
            size = self.ui_handle_config()
            self.__style = Style(self)
        except:
            # rollback
            for option, value in items:
                setattr(self, "ui_option_" + option, values.pop(0))
            raise

        margin = self.__style.getmargin()
        self._ui_driver._setmargin(margin)

        if size or default_size:
            if not size:
                size = default_size
            margin = 2 * margin
            self.tk.call(
                self, "config",
                "-width", size[0] + margin, "-height", size[1] + margin
                )

        if options.has_key("cursor"):
            self.tk.call(
                self, "config",
                "-cursor", str(self.ui_option_cursor)
                )

        return size

    ##
    # Configures a widget instance.
    #
    # @def config(**options)
    # @param **options One or more options, given as keyword arguments.

    def configure(self, **options):
        "Set one or more widget options"
        if not options:
            # FIXME
            raise NotYetImplemented, "configure query not yet implemented"
        self.ui_damage()
        return self.__configure(options)

    config = configure

    ##
    # Gets the value of a widget configuration option.
    #
    # @param option What option value to fetch.
    # @return The option value.
    # @exception AttributeError The option was not supported.

    def cget(self, option):
        "Get option value"
        try:
            return getattr(self, "ui_option_" + option)
        except AttributeError:
            raise Error, "unsupported option: %s" % option

    __getitem__ = cget

    ##
    # Sets the value of a single configuration option.
    #
    # @param option What option to modify.
    # @param value The new value for this option.

    def __setitem__(self, option, value):
        self.configure({option: value})

    ##
    # Gets a list of all available options.
    #
    # @return A list of option names.

    def keys(self):
        "Get available options"
        return self.configure().keys()

    ##
    # (Experimental) Attach a geometry manager to this widget.
    #
    # @param manager What manager to use.
    # @param **options Manager options, given as keyword arguments.

    def manage(self, manager="pack", **options):
        "Manage this widget using the given manager (pack, place, grid)"
        self.tk.call((manager, "config", str(self)) + self._options(options))

    #
    # construction kit api

    ##
    # (Hook) Called by the framework to clear a portion of this
    # widget.  The default implementations fills the background with
    # the current background style.  If you're drawing the background
    # in the repair method, you should override this method with an
    # empty implementation.
    #
    # @param draw A drawing context.  This is an object implementing
    #    the {@link DrawInterface} interface.
    # @param x0,y0,x1,y1 What region to clear.  This region usually
    #     covers the entire widget.

    def ui_handle_clear(self, draw, x0, y0, x1, y1):
        "Draw widget background"
        self.__style.drawbackground((x0, y0, x1, y1), draw)

    ##
    # (Hook) Called by the framework when this widget has been
    # reconfigured.  This method should check configuration options
    # (ui_option attributes), and update widget attributes as
    # necessary.
    #
    # @return A 2-tuple giving the width and height, in pixels, or
    #    None to preserve the current size.  When the widget is first
    #    created, the current size is set to 100x100 pixels.

    def ui_handle_config(self):
        "Configuration options changed"
        pass

    ##
    # (Hook) Called by the framework when some part of this widget has
    # been damaged, and will have to be redrawn.  This method will
    # always be called at least once before each call to
    # <b>ui_handle_repair</b>.
    #
    # @param x0,y0,x1,y1 The damaged region.

    def ui_handle_damage(self, x0, y0, x1, y1):
        "Register damage"
        pass

    ##
    # (Hook) Called by the framework when this widget is about to be
    # destroyed.

    def ui_handle_destroy(self):
        "Destroy widget"
        pass

    ##
    # (Hook) Called by the framework when this widget has received or
    # is about to loose focus.
    #
    # @param draw A drawing context.  This is an object implementing
    #    the {@link DrawInterface} interface.
    # @param has_focus A true value if the widget has just received
    #    focus, a false value if it is about to loose focus.

    def ui_handle_focus(self, draw, has_focus):
        "Focus changed"
        pass

    ##
    # (Hook) Called by the framework when this widget should be
    # redrawn.  This call will always be preceeded by one or more
    # calls to <b>ui_handle_damage</b>, and a single call to
    # <b>ui_handle_clear</b>.
    #
    # @param draw A drawing context.  This is an object implementing
    #    the {@link DrawInterface} interface.
    # @param x0,y0,x1,y1 What region to redraw.  This region usually
    #     covers the entire widget.  To redraw only portions of the
    #     widget, override the <b>ui_handle_damage</b> method and keep
    #     track of the damaged region.

    def ui_handle_repair(self, draw, x0, y0, x1, y1):
        "Repair/redraw widget"
        pass

    ##
    # (Hook) Called by the framework when this widget has been
    # resized, either by a geometry manager, or by the user.
    #
    # @param width The new width, in pixels.
    # @param height The new height, in pixels.

    def ui_handle_resize(self, width, height):
        "Update geometry"
        pass

    ##
    # (Hook) Called by the framework when this selection status has
    # changed.  In the current implementation, this handler is never
    # called.

    def ui_handle_select(self):
        pass # NYI

    ##
    # Reports widget damage.  This will force the widget to redraw all
    # or parts of it's screen estate.
    #
    # @param x0,y0,x1,y1 What region to redraw.  If omitted, the
    #    entire widget will be redrawn.

    def ui_damage(self, x0=None, y0=None, x1=None, y1=None):
        "Report widget damage"
        if not self.__visible:
            return
        if x0 is None:
            x0, y0, x1, y1 = (0, 0) + self.__size
        elif isinstance(x0, type(())):
            x0, y0, x1, y1 = x0
        if self.__repair_pending:
            if x1 > x0 or y1 > x0:
                self.ui_handle_damage(x0, y0, x1, y1)
        else:
            self.__repair_pending = 1
            self.tk.call(
                "event", "generate", self, "<Expose>",
                "-x", x0, "-y", y0, "-width", x1-x0, "-height", y1-y0,
                "-when", "tail"
                )

    ##
    # Sets the controller for this widget.
    #
    # @param controller Controller class.
    # @return Controller instance, or None.

    def ui_setcontroller(self, controller=None):
        "Set controller"
        tags = list(self.bindtags())
        try:
            tags.remove(self.__controller)
            _controller_tags[self.__controller].detach(self)
        except ValueError:
            pass
        c = getcontroller(controller, self)
        if c:
            self.__controller = c.tag
            tags.insert(1, c.tag)
            self.bindtags(tuple(tags))
            c.attach(self)
        else:
            self.__controller = None
        return c

    ##
    # Gets the current size of this widget.
    #
    # @return The size in pixels, as a (width, height) 2-tuple.

    def ui_size(self):
        "Get current widget (view) size"
        return self.__size


    def _ui_border(self, draw):
        # internal: redraw border
        if self.__style:
            self.__style.drawborder(None, draw)

    def _ui_setsize(self, width, height):
        # internal: set size
        margin = 2 * self.__style.getmargin()
        self.__size = width - margin, height - margin
        return self.__size

    def _ui_setvisible(self, visible):
        # internal: set visibility flag
        self.__visible = visible

    def _ui_reset_repair_pending(self):
        # internal: reset repair pending flag
        self.__repair_pending = 0

    #
    # object factories

    ##
    # Creates a pen object with the given characteristics.  The pen
    # can only be used in this widget.
    #
    # @param color What colour to use.  This can be a colour
    #    name, a hexadecimal colour specifier ("#rrggbb"), or
    #    a packed integer (0xrrggbb).
    # @param width Pen width, in pixels.
    # @param **options Additional options (device specific).

    def ui_pen(self, color="black", width=1, **options):
        "Create pen object"
        key = "pen", color, width
        try:
            return self.__cache[key]
        except (KeyError, TypeError):
            pen = self._ui_driver.getpen(color, width)
            if self.__cache is not None:
                self.__cache[key] = pen
            return pen

    ##
    # Creates a brush object with the given characteristics.  The
    # brush can only be used in this widget.
    #
    # @param color What colour to use.  This can be a colour
    #    name, a hexadecimal colour specifier ("#rrggbb"), or
    #    a packed integer (0xrrggbb).
    # @param **options Additional options (device specific).

    def ui_brush(self, color="black", **options):
        "Create brush object"
        key = "brush", color
        try:
            return self.__cache[key]
        except (KeyError, TypeError):
            brush = self._ui_driver.getbrush(color)
            if self.__cache is not None:
                self.__cache[key] = brush
            return brush

    ##
    # Creates a font object with the given characteristics, for
    # use in this widget.
    #
    # @param color What colour to use.  This can be a colour
    #    name, a hexadecimal colour specifier ("#rrggbb"), or
    #    a packed integer (0xrrggbb).
    # @param font A font specifier.  This should be a string
    #    with the following syntax: "{family} size style...".
    # <p>
    # If the family name doesn't contain whitespace, you can leave
    # out the braces.  If omitted, the family name defaults to
    # <b>courier</b>.
    # <p>
    # The <i>size</i> is given in points (defined as 1/72 inch).  If
    # omitted, it defaults to 12 points.  Note that the toolkit takes
    # the <i>logical screen size</i> into account when calculating the
    # actual font size.  On low resolution screens, this means that a
    # 12-point font is usually larger than 12/72 inches.
    # <p>
    # The <i>style</i> attributes can be any combination of
    # <b>normal</b>, <b>bold</b>, <b>roman</b> (upright),
    # <b>italic</b>, <b>underline</b>, and <b>overstrike</b>.  If
    # omitted, it defaults to the default setting for that family;
    # usually <b>normal roman</b>.
    # <p>
    # For Tkinter compatibility, you can also give the font as a
    # tuple: ("family", size, style...).   Note that there should
    # be no braces around the family name.  You can also leave out
    # the size and/or the style arguments.  The defaults are the
    # same as for the string syntax.
    # @return A font object.

    def ui_font(self, color="black", font="Courier", **options):
        "Create font object"
        key = color, font
        try:
            return self.__fontcache[key]
        except (KeyError, TypeError):
            font = self._ui_driver.getfont(color, font)
            if self.__fontcache is not None:
                self.__fontcache[key] = font
            return font

    ##
    # Creates an image object with the given characteristics.  The
    # image (or a cropped subregion of it) can be pasted onto a a
    # window or a pixmap.
    # <p>
    # Note that image descriptors are not cached.
    # <p>
    # Also note that in the current version, this method returns a
    # pixmap.  This may change in future versions.
    #
    # @param image The source image.  This can be a PIL <b>Image</b>
    #     object, or a Tkinter <b>BitmapImage</b> or <b>PhotoImage</b>
    #     object.  Alternatively, you can pass in a <b>fromstring</b>-style
    #     mode string, a size tuple, and a string containing the pixel data.
    # @return An image object (currently a pixmap).

    def ui_image(self, image=None, size=None, data=None, **options):
        "Create image object"
        if image and size and data:
            mode = image
            image = Tkinter.PhotoImage()
            self._ui_driver.setimage(str(image), mode, size, data)
        elif hasattr(image, "im"):
            from PIL import ImageTk
            image = ImageTk.PhotoImage(image)
        return self._ui_driver.getimage(str(image))

    ##
    # Creates an pixmap object with the given characteristics.
    #
    # @param width The width of the pixmap, in pixels.
    # @param height The height of the pixmap, in pixels.
    # @param **options Additional options (device dependent).
    # @return A pixmap object.  This is an object implementing
    #    the {@link DrawInterface} interface.

    def ui_pixmap(self, width, height, **options):
        "Create pixmap object"
        return self._ui_driver.getpixmap(width, height)

    ##
    # Converts a coordinate list to a more efficient representation.
    # This method can be used to "compile" coordinate lists.
    #
    # @param xy Coordinate list.
    # @return A path object.  Note that this may be a reference to the
    #     input list, for WCK platforms that do no support path
    #     compilation.

    def ui_path(self, xy):
        "Create compile path object"
        try:
            return self._ui_driver.getpath(xy)
        except AttributeError:
            return xy

    ##
    # Clears the resource cache.  Clears the cache for this
    # widget (and any other widgets that may share the same
    # cache).  Resources that are cached as instance attributes
    # are not affected.
    # <p>
    # If this method is called from the constructor, before the
    # <b>ui_init</b> is called, caching is disabled for this
    # widget.  Widgets that use large numbers of resources (e.g.
    # font and colour browsers) should disable the cache.

    def ui_purge(self):
        "Clear resource cache"
        if hasattr(self, "ui_view"):
            self.__cache.clear()
        else:
            # disable caching if called before ui_init
            self.__cache = None

##
# Controller base class.  This class provides a standard
# implementation of the controller class.  To implement a custom
# controller, create a subclass to this class and implement the
# <b>create</b> method.

class Controller:

    ##
    # Binding tag for this controller.

    tag = None
    __created = 0

    ##
    # (Internal) Calculate a unique binding tag for this controller
    # instance.

    def ui_gettag(self, widget):
        # internal: lazy instantation
        if not self.tag:
            self.tag = "Tkinter3000.Controller.%d" % id(self)
        if not self.__created:
            # local binding function
            def handle(event, function, tag=self.tag, widget=widget):
                widget.bind_class(tag, event, function)
            self.create(handle)
            self.__created = 1
        return self.tag

    ##
    # Creates a controller.  This method must be overridden by the
    # implementation class.
    #
    # @param handle A binding function that takes an event specifier
    #     (a string) and an event handler (a function).  The handler
    #     will be called with an event structure for all matching
    #     events.

    def create(self, handle):
        raise NotImplementedError, "must override create"

    ##
    # (Hook) Called when this controller is attached to the given widget.
    #
    # @param widget The widget instance.

    def attach(self, widget):
        pass

    ##
    # (Hook) Called when this controller is detached from the given widget.
    #
    # @param widget The widget instance.

    def detach(self, widget):
        pass

class _DefaultController(Controller):
    # internal: this controller is attached to all WCK widgets

    tag = "Tkinter3000.DefaultController"

    def create(self, handle):
        handle("<Configure>", self.handle_configure)
        handle("<Destroy>", self.handle_destroy)
        handle("<Expose>", self.handle_expose)
        handle("<FocusIn>", self.handle_focusin)
        handle("<FocusOut>", self.handle_focusout)
        handle("<Map>", self.handle_map)
        handle("<Unmap>", self.handle_unmap)

    def handle_configure(self, event):
        widget = event.widget
        width, height = widget._ui_setsize(event.width, event.height)
        widget.ui_draw = widget._ui_driver._resize(width, height)
        widget.ui_handle_resize(width, height)

    def handle_destroy(self, event):
        widget = event.widget
        if isinstance(widget, Widget):
            widget._ui_destroy()

    def handle_expose(self, event):
        widget = event.widget
        x0 = event.x; y0 = event.y
        if event.width > 0 or event.height > 0:
            widget.ui_handle_damage(x0, y0, x0+event.width, y0+event.height)
## FIXME: event.count is not yet supported by Tkinter
##         try:
##             count = event.count
##         except AttributeError:
##             count = 0
##        if not count:
        draw = widget.ui_draw
        driver = widget._ui_driver
        width, height = widget.ui_size()
        widget._ui_reset_repair_pending()
        if widget.ui_doublebuffer:
            pixmap = driver.getpixmap(width, height)
            widget.ui_handle_clear(pixmap, 0, 0, width, height)
            widget.ui_handle_repair(pixmap, 0, 0, width, height)
            widget._ui_border(pixmap)
            draw.paste(pixmap)
        else:
            widget.ui_handle_clear(draw, 0, 0, width, height)
            widget.ui_handle_repair(draw, 0, 0, width, height)
            widget._ui_border(draw)
        draw.settransform() # reset
        widget._ui_driver._flush()

    def handle_focusin(self, event):
        widget = event.widget
        widget.ui_handle_focus(widget.ui_draw, 1)

    def handle_focusout(self, event):
        widget = event.widget
        widget.ui_handle_focus(widget.ui_draw, 0)

    def handle_map(self, event):
        widget = event.widget
        widget._ui_setvisible(1)

    def handle_unmap(self, event):
        widget = event.widget
        widget._ui_setvisible(0)

_controller_registry = {}
_controller_tags = {}

##
# Creates a controller for the given widget.
#
# @param controller_factory A controller factory or class (usually a
#     subclass to {@link Controller}, or a compatible class).
# @param widget The widget to create a controller for.
# @return A controller instance.  The controller is initialized, and
#     has a valid <b>tag</b> attribute.

def getcontroller(controller_factory, widget):
    "Create a controller for the given widget"
    try:
        controller =_controller_registry[controller_factory]
    except KeyError:
        if not callable(controller_factory):
            return None # no controller
        controller = controller_factory()
        if controller is None:
            return None # cannot create controller
        _controller_registry[controller_factory] = controller
        _controller_tags[controller.ui_gettag(widget)] = controller
        assert controller.tag
    return controller

