/*
 * Tkinter 3000 Widget Construction Kit
 * $Id: /work/modules/tkinter3000/tk3000.h 170 2004-04-09T10:10:46.102102Z fredrik  $
 *
 * public Tkinter 3000 WCK services
 *
 * Copyright (c) 2002-2004 by Secret Labs AB
 * Copyright (c) 2002-2004 by Fredrik Lundh
 */

/* make sure Python.h is included before including this file! */

#define WCK_SIMPLE_2D_API "WCKSimple2DAPI"
#define WCK_SIMPLE_2D_API_VERSION 1

typedef struct WCKSimple2DAPI {

    int version; /* should be set to WCK_SIMPLE_2D_API_VERSION */
    struct WCKSimple2DAPI *fallback;

    /* Simple2DCore.  All 2D implementations must implement
       this group of operations. */

    int (*line)(
        PyObject* draw, int x0, int y0, int x1, int y1,
        PyObject* pen
        );

    int (*rectangle)(
        PyObject* draw, int x0, int y0, int x1, int y1,
        PyObject* pen, PyObject* brush
        );

    PyObject* (*getpen)(
        PyObject* draw, int color, int width
        );

    PyObject* (*getbrush)(
        PyObject* draw, int color, int style
        );

    int (*text)(
        PyObject* draw, int x0, int y0, char* text, int textsize,
        PyObject* font
        );

    int (*textsize)(
        PyObject* draw, char* text, int textsize, PyObject* font,
        int* width, int* height
        );

    PyObject* (*getfont)(
        PyObject* draw, int color, char* fontname
        );

    /* Simple2DDraw */

    /* TBD */

} WCKSimple2DAPI;
