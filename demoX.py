#
# Tkinter 3000
#
# using an external widget implementation (skeleton code)
#

from WCK import Widget

class XWidget(Widget):

    def ui_handle_resize(self, width, height):
        print "resize", self.winfo_id(), width, height

    def ui_handle_damage(self, x0, y0, x1, y1):
        print "damage", self.winfo_id(), x0, y0, x1, y1

    def ui_handle_repair(self, draw, x0, y0, x1, y1):
        print "repair", self.winfo_id(), x0, y0, x1, y1

if __name__ == "__main__":

    import Tkinter

    root = Tkinter.Tk()
    root.title("demoX")

    widget = XWidget(root)
    widget.pack(fill="both", expand=1)

    root.mainloop()

