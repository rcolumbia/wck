#
# Tkinter 3000
#
# display a line of text
#

from WCK import Widget, TextMixin

class TextLabel(TextMixin, Widget):

    ui_option_text = ""

    def ui_text_get(self):
        return str(self.ui_option_text)

    # ui_handle_config provided by TextMixin

    def ui_handle_repair(self, draw, x0, y0, x1, y1):
        self.ui_text_draw(draw)

if __name__ == "__main__":

    import Tkinter

    root = Tkinter.Tk()
    root.title("demoTextLabel")

    widget = TextLabel(root, text="TextLabel")
    widget.pack(fill="both", expand=1)

    root.mainloop()
