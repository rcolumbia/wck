#
# Tkinter 3000 -- Widget Construction Kit
# $Id: /work/modules/tkinter3000/tk3/__init__.py 170 2004-04-09T10:10:46.102102Z fredrik  $
#
# compatibility layer.  new code should import WCK instead
# (or the appropriate driver)

import warnings
warnings.warn("the tk3 module is deprecated; please use WCK instead",
              DeprecationWarning)

from WCK.wckTkinter import *
from WCK.Utils import *
