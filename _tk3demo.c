/*
 * Tkinter 3000 Widget Construction Kit
 * $Id: /work/modules/tkinter3000/_tk3demo.c 170 2004-04-09T10:10:46.102102Z fredrik  $
 *
 * Demo extensions.
 *
 * history:
 * 2002-08-18 fl   widget C API demo
 *
 * Copyright (c) 2002 by Secret Labs AB
 * Copyright (c) 2002 by Fredrik Lundh
 *
 * info@pythonware.com
 * http://www.pythonware.com
 */

/* --------------------------------------------------------------------
 * The Tkinter 3000 Widget Construction Kit is
 *
 * Copyright (c) 1998-2002 by Secret Labs AB
 * Copyright (c) 1998-2002 by Fredrik Lundh
 *
 * By obtaining, using, and/or copying this software and/or its
 * associated documentation, you agree that you have read, understood,
 * and will comply with the following terms and conditions:
 *
 * Permission to use, copy, modify, and distribute this software and its
 * associated documentation for any purpose and without fee is hereby
 * granted, provided that the above copyright notice appears in all
 * copies, and that both that copyright notice and this permission notice
 * appear in supporting documentation, and that the name of Secret Labs
 * AB or the author not be used in advertising or publicity pertaining to
 * distribution of the software without specific, written prior
 * permission.
 *
 * SECRET LABS AB AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO
 * THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS.  IN NO EVENT SHALL SECRET LABS AB OR THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 * -------------------------------------------------------------------- */

#include <stdio.h>

#include "Python.h"

#include "tk3000.h"

/* -------------------------------------------------------------------- */
/* sample C widget for the WCK */

typedef struct {
    PyObject_HEAD
    /* attributes */
    int width;
    int height;
    PyObject* draw;
    PyObject* pen;
    WCKSimple2DAPI* capi; /* FIXME */
} WidgetObject;

staticforward PyTypeObject Widget_Type;

static PyObject* 
module_widget(PyObject* self, PyObject* args)
{
    WidgetObject* widget;
    char* api_type;
    void* capi;

    PyObject* draw;
    if (!PyArg_ParseTuple(args, "OO:widget", &draw, &capi))
        return NULL;

    widget = PyObject_NEW(WidgetObject, &Widget_Type);
    if (!widget)
	return NULL;

    Py_INCREF(draw);
    widget->draw = draw;

    widget->pen = NULL;

    /* get C API hook */
    /* FIXME: provide standard macro for this? */
    api_type = PyCObject_GetDesc(capi);
    if (!api_type || strcmp(api_type, WCK_SIMPLE_2D_API)) {
        PyErr_SetString(
            PyExc_TypeError, "invalid draw object"
            );
        return NULL;
    }

    widget->capi = PyCObject_AsVoidPtr(capi);
    if (!widget->capi)
        return NULL;

    widget->width = widget->height = 1; /* unknown */

    return (PyObject*) widget;
}

static PyObject *
widget_resize(WidgetObject* widget, PyObject* args)
{
    int width, height;
    if (!PyArg_ParseTuple(args, "ii:resize", &width, &height))
        return NULL;

    widget->width = width;
    widget->height = height;

    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
widget_repair(WidgetObject* widget, PyObject* args)
{
    int x, width, height;
    PyObject* pen;
    PyObject* font;

    int x0, y0, x1, y1;
    if (!PyArg_ParseTuple(args, "iiii:repair", &x0, &y0, &x1, &y1))
        return NULL;

    pen = widget->capi->getpen(widget->draw, 0, 0);

    for (x = 0; x < widget->width; x += 2)
        widget->capi->line(
            widget->draw, 0, 0, x, widget->height, pen
            );

    for (x = 0; x < widget->width; x += 2)
        widget->capi->line(
            widget->draw, widget->width, widget->height, x, 0, pen
            );

    Py_DECREF(pen);

    font = widget->capi->getfont(widget->draw, 0xffffff, "helvetica");

    widget->capi->textsize(widget->draw, "C", 1, font, &width, &height);

    widget->capi->text(
        widget->draw, (widget->width - width) / 2, 10, "C", 1, font
        );

    Py_DECREF(font);

    Py_INCREF(Py_None);
    return Py_None;
}

static void
widget_dealloc(WidgetObject* self)
{
    Py_XDECREF(self->draw);
    Py_XDECREF(self->pen);
    PyMem_DEL(self);
}

static PyMethodDef widget_methods[] = {
    {"repair", (PyCFunction)widget_repair, 1},
    {"resize", (PyCFunction)widget_resize, 1},
    {NULL, NULL}
};

static PyObject*  
widget_getattr(WidgetObject* widget, char* name)
{
    return Py_FindMethod(widget_methods, (PyObject*) widget, name);
}

statichere PyTypeObject Widget_Type = {
    PyObject_HEAD_INIT(NULL)
    0, "Widget", sizeof(WidgetObject), 0,
    (destructor)widget_dealloc, /* tp_dealloc */
    0, /* tp_print */
    (getattrfunc)widget_getattr, /* tp_getattr */
};

/* -------------------------------------------------------------------- */

static PyMethodDef module_functions[] = {
    {"widget", (PyCFunction)module_widget, 1},
    {NULL, NULL}
};

void
#ifdef WIN32
__declspec(dllexport)
#endif
init_tk3demo(void)
{
    Py_InitModule("_tk3demo", module_functions);
}
