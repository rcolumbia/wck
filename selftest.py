# $Id: /work/modules/tkinter3000/selftest.py 451 2004-09-22T19:37:23.860729Z fredrik  $
# -*- coding: ascii -*-
# tkinter3000 selftest program

import WCK

def sanity():
    """
    >>> import _tk3draw
    >>> import _tk3demo
    >>> import WCK
    >>> WCK.VERSION
    '1.1a2'
    """

class UI15Controller(WCK.Controller):
    def create(self, hook):
        pass

class UI15Widget1(WCK.Widget):
    ui_controller = UI15Controller

class UI15Widget2(WCK.Widget):
    pass

def uitoolkit15():
    """
    Controller reuse.

    >>> w1 = UI15Widget1(None)
    >>> w2 = UI15Widget2(None)

    >>> if w1.ui_controller == UI15Controller:
    ...     print "ok"
    ok
    >>> if w2.ui_controller == None:
    ...     print "ok"
    ok

    >>> w2.destroy()
    >>> w1.destroy()

    """

class UI16Controller1(WCK.Controller):
    def create(self, hook):
        pass
    def attach(self, widget):
        print "ATTACH 1"
    def detach(self, widget):
        print "DETACH 1"

class UI16Controller2(WCK.Controller):
    def create(self, hook):
        pass
    def attach(self, widget):
        print "ATTACH 2"
    def detach(self, widget):
        print "DETACH 2"

class UI16Widget(WCK.Widget):
    ui_controller = UI16Controller1

def uitoolkit16():
    """
    Attach/detach hooks.

    >>> w = UI16Widget(None)
    ATTACH 1
    >>> x = w.ui_setcontroller(UI16Controller1)
    DETACH 1
    ATTACH 1
    >>> x = w.ui_setcontroller(UI16Controller2)
    DETACH 1
    ATTACH 2
    >>> w.destroy()
    DETACH 2

    """

if __name__ == "__main__":
    import doctest, selftest
    failed, tested = doctest.testmod(selftest)
    print tested - failed, "tests ok."
