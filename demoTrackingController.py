#
# Tkinter 3000
#
# tracking controller demo
#

from WCK import Widget, Controller

class SelectTracker(Controller):

    def create(self, handle):
        handle("<Button-1>", self.click)

    def click(self, event):
        widget = event.widget
        widget.anchor = event.x, event.y
        if widget.xy:
            x0, y0, x1, y1 = widget.xy
            if x0 <= event.x < x1 and y0 <= event.y < y1:
                widget.ui_setcontroller(MoveTracker)
                return
        widget.ui_setcontroller(DrawTracker)

class MoveTracker(Controller):

    def create(self, handle):
        handle("<B1-Motion>", self.move)
        handle("<ButtonRelease-1>", self.release)

    def move(self, event):
        widget = event.widget
        dx = event.x - widget.anchor[0]
        dy = event.y - widget.anchor[1]
        x0, y0, x1, y1 = widget.xy
        widget.xy = x0 + dx, y0 + dy, x1 + dx, y1 + dy
        widget.anchor = event.x, event.y
        widget.ui_damage()

    def release(self, event):
        widget = event.widget
        widget.ui_setcontroller(SelectTracker)

class DrawTracker(Controller):

    def create(self, handle):
        handle("<B1-Motion>", self.draw)
        handle("<ButtonRelease-1>", self.release)

    def draw(self, event):
        widget = event.widget
        x0 = min(widget.anchor[0], event.x)
        y0 = min(widget.anchor[1], event.y)
        x1 = max(widget.anchor[0], event.x)
        y1 = max(widget.anchor[1], event.y)
        widget.xy = x0, y0, x1, y1
        widget.ui_damage()

    def release(self, event):
        widget = event.widget
        widget.ui_setcontroller(SelectTracker)

##
# Draws a filled rectangle between two points.

class RectangleWidget(Widget):

    ui_option_width = ui_option_height = 500

    ui_controller = SelectTracker

    xy = None

    def ui_handle_config(self):
        return int(self.ui_option_width), int(self.ui_option_height)

    def ui_handle_repair(self, draw, x0, y0, x1, y1):
        if self.xy:
            draw.rectangle(self.xy, self.ui_brush("red"))


if __name__ == "__main__":

    import Tkinter

    root = Tkinter.Tk()
    root.title("demoTrackingController")

    Tkinter.Label(
        root, text="use left button to draw a new rectangle,"
        " or to move the existing rectangle"
        ).pack()

    widget = RectangleWidget(root)
    widget.pack(fill="both", expand=1)

    root.mainloop()
