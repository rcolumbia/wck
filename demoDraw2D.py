# $Id: /work/modules/tkinter3000/demoDraw2D.py 430 2004-09-18T20:48:37.152345Z fredrik  $
# -*- coding: iso-8859-1 -*-
#
# Tkinter 3000
#
# draw graphics using the standard simple 2D interface
#

import sys, time

from WCK import Widget

BENCHMARK = "-b" in sys.argv

class Drawer:

    def repair(self, draw):

        try:
            draw.text((0, 0), repr(draw), self.ui_font("black", "courier 8"))
        except:
            pass

        pen = self.ui_pen("black")
        brush = self.ui_brush("gold")

        bluepen = self.ui_pen("blue", 3)

        try:
            draw.line((50, 50, 100, 100), pen)
            draw.line((50, 80, 100, 130), self.ui_pen("black", 5))
        except AttributeError, v:
            print "*** cannot draw lines on", draw

        try:
            draw.rectangle((50, 150, 100, 200), pen)
            draw.rectangle((50, 220, 100, 270), brush)
            draw.rectangle((50, 290, 100, 340), brush, pen)
            draw.rectangle((50, 360, 100, 410), brush, bluepen)
        except AttributeError, v:
            print "*** cannot draw rectangles on", draw

        try:
            draw.ellipse((120, 150, 170, 200), pen)
            draw.ellipse((120, 220, 170, 270), brush)
            draw.ellipse((120, 290, 170, 340), brush, pen)
            draw.ellipse((120, 360, 170, 410), brush, bluepen)
        except AttributeError, v:
            print "*** cannot draw ellipses on", draw

        try:
            draw.polygon((190+25, 150, 190, 200, 190+50, 200), pen)
            draw.polygon((190+25, 220, 190, 270, 190+50, 270), brush)
            draw.polygon((190+25, 290, 190, 340, 190+50, 340), brush, pen)
            draw.polygon((190+25, 360, 190, 410, 190+50, 410), brush, bluepen)
        except AttributeError, v:
            print "*** cannot draw polygons on", draw

        # from demoUnicode.py

        font = self.ui_font()
        x = 120
        y = 50
        for text in (
             "     ascii: abc",
             "iso-8859-1: ���",
            u"     utf-8: ���".encode("utf-8"),
            u"   unicode: ���"):
            try:
                draw.text((x, y), text, font)
                w, h = draw.textsize(text, font)
                y = y + h
                # print draw.textsize(text, font), draw.textsize(None, font)
            except (TypeError, ValueError, AttributeError):
                print "*** cannot draw", repr(text), "on", draw

class DemoWidget(Drawer, Widget):

    ui_option_background = "white"
    ui_option_relief = "sunken"
    ui_option_borderwidth = 1

    def ui_handle_config(self):
        return 500, 500

    def ui_handle_repair(self, draw, x0, y0, x1, y1):
        if BENCHMARK:
            t0 = time.time()
            for i in range(1000):
                self.repair(draw)
            print draw, time.time() - t0
        else:
            self.repair(draw)

class WinDrawDemoWidget(DemoWidget):

    draw = None

    def ui_pen(self, color="black", width=1):
        return self.draw.getpen(self.winfo_rgb(color), width)

    def ui_brush(self, color="black"):
        if self.draw is None:
            return Widget.ui_brush(self, color)
        return self.draw.getbrush(self.winfo_rgb(color))

    def ui_font(self, color="black", font="courier"):
        return self.draw.getfont(self.winfo_rgb(color), font)

    def ui_handle_repair(self, draw, x0, y0, x1, y1):
        import _windraw
        self.draw = draw = _windraw.getdraw(self.winfo_id())
        print self.winfo_id(), draw
        if BENCHMARK:
            t0 = time.time()
            for i in range(1000):
                self.repair(draw)
            print draw, time.time() - t0
        else:
            self.repair(draw)
        self.draw = None

if __name__ == "__main__":

    import Tkinter

    root = Tkinter.Tk()
    root.title("demoDraw2D")

    widget = DemoWidget(root)
    widget.pack(side="left", fill="both", expand=1)

    try:
        import _windraw
    except ImportError:
        pass
    else:
        widget = WinDrawDemoWidget(root)
        widget.pack(side="left", fill="both", expand=1)

    root.mainloop()
