from WCK import *

class ModelDamage:
    pass

class CanvasModel(Observable):

    def __init__(self):
        self.items = []

    def append(self, box):
        self.items.append(box)
        self.notify(ModelDamage())

    def repair(self, widget, draw):
        brush = widget.ui_brush("red")
        pen = widget.ui_pen("black")
        for item in self.items:
            draw.rectangle(item, brush, pen)

class CanvasWidget(Widget):

    ui_option_width = 800
    ui_option_height = 600

    def __init__(self, master, **options):
        self.model = CanvasModel()
        self.model.addobserver(self.notify)
        self.ui_init(master, options)

    def notify(self, event, data):
        if isinstance(event, ModelDamage):
            self.ui_damage()

    def ui_handle_destroy(self):
        self.model.removeobserver(self.notify)

    def ui_handle_config(self):
        return int(self.ui_option_width), int(self.ui_option_height)

    def ui_handle_repair(self, draw, x0, y0, x1, y1):
        self.model.repair(self, draw)

if __name__ == "__main__":

    import Tkinter

    root = Tkinter.Tk()
    root.title("demoSimpleCanvas")

    widget = CanvasWidget(root)
    widget.pack(fill="both", expand=1)

    def add(event):
        box = event.x-10, event.y-10, event.x+10, event.y+10
        widget.model.append(box)

    def move(event):
        del widget.model.items[-1]
        add(event)

    widget.bind("<Button-1>", add)
    widget.bind("<B1-Motion>", move)

    widget.model.append((10, 10, 100, 100))
    widget.model.append((50, 10, 200, 30))
    widget.model.append((80, 50, 300, 120))

    root.mainloop()


