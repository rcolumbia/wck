#
# Tkinter 3000
#
# drawing lots of dots
#

from WCK import Widget

from Tkinter import Canvas
class SimplePlot1(Canvas):

    def plot(self, x, y):
        self.create_line((x, y, x+1, y), fill="black")


class SimplePlot2(Widget):

    ui_option_foreground = "black"

    ui_option_width = 200
    ui_option_height = 200

    def ui_handle_config(self):
        self.brush = self.ui_brush(self.ui_option_foreground)
        return int(self.ui_option_width), int(self.ui_option_height)

    def plot(self, x, y):
        self.ui_draw.rectangle((x, y, x+1, y+1), self.brush)


class SimplePlot3(Widget):

    ui_option_foreground = "black"

    ui_option_width = 200
    ui_option_height = 200

    def __init__(self, master, **options):
        self.data = []
        self.ui_init(master, options)

    def plot(self, x, y):
        self.data.append((x, y, x+1, y+1))
        self.ui_damage()

    def ui_handle_config(self):
        self.brush = self.ui_brush(self.ui_option_foreground)
        return int(self.ui_option_width), int(self.ui_option_height)

    def ui_handle_repair(self, draw, x0, y0, x1, y1):
        rectangle = draw.rectangle
        brush = self.brush
        for xy in self.data:
            rectangle(xy, brush)


class SimplePlot4(Widget):

    ui_option_foreground = "black"

    ui_option_width = 200
    ui_option_height = 200

    def __init__(self, master, **options):
        self.data = []
        self.data_size = 0
        self.ui_init(master, options)
        self.tick()

    def tick(self):
        data_size = len(self.data)
        if data_size != self.data_size:
            self.ui_damage()
            self.data_size = data_size
        self.after(100, self.tick)

    def plot(self, x, y):
        self.data.append((x, y, x+1, y+1))

    def ui_handle_config(self):
        self.brush = self.ui_brush(self.ui_option_foreground)
        return int(self.ui_option_width), int(self.ui_option_height)

    def ui_handle_repair(self, draw, x0, y0, x1, y1):
        rectangle = draw.rectangle
        brush = self.brush
        for xy in self.data:
            rectangle(xy, brush)


if __name__ == "__main__":

    import Tkinter, random, time

    root = Tkinter.Tk()
    root.title("demoSimplePlot")

    widget = SimplePlot4(root, width=200, height=200)
    widget.pack(fill="both", expand=1)

    widget.update() # display it

    data = []
    for i in range(5000):
        data.append((random.randint(0, 200), random.randint(0, 200)))

    t0 = time.time()
    for x, y in data:
        widget.plot(x, y)
    widget.update() # make sure everything is drawn
    print time.time() - t0, "seconds"

    root.mainloop()

