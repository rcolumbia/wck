#
# Tkinter 3000
#
# this demo shows how to implement a simple image viewer.

from WCK import Widget

import Image # requires PIL
import base64

class ImageView(Widget):

    ui_option_width = 128
    ui_option_height = 128

    def __init__(self, master, **options):
        self.image = self.pixmap = None
        self.ui_init(master, options)

    def ui_handle_config(self):
        return (
            int(self.ui_option_width),
            int(self.ui_option_height)
        )

    def ui_handle_repair(self, draw, x0, y0, x1, y1):
        if self.image is None:
            return
        if self.pixmap is None:
            # delay loading until the window exists
            im = self.image
            self.pixmap = self.ui_image(im.mode, im.size, im.tostring())
        draw.paste(self.pixmap)

    def setimage(self, image):
        self.image = image
        self.pixmap = None
        self.ui_damage()

# demo code

image = Image.fromstring("RGB", (128, 128), base64.decodestring("""
/9j/4AAQSkZJRgABAQAAAQABAAD//gBIQ1JFQVRPUjogWFYgVmVyc2lvbiAzLjEwYSAgUmV2OiAx
Mi8yOS85NCAgUXVhbGl0eSA9IDc1LCBTbW9vdGhpbmcgPSAwCv/bAEMACAYGBwYFCAcHBwkJCAoM
FA0MCwsMGRITDxQdGh8eHRocHCAkLicgIiwjHBwoNyksMDE0NDQfJzk9ODI8LjM0Mv/bAEMBCQkJ
DAsMGA0NGDIhHCEyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIy
MjIyMv/AABEIAIAAgAMBIgACEQEDEQH/xAAfAAABBQEBAQEBAQAAAAAAAAAAAQIDBAUGBwgJCgv/
xAC1EAACAQMDAgQDBQUEBAAAAX0BAgMABBEFEiExQQYTUWEHInEUMoGRoQgjQrHBFVLR8CQzYnKC
CQoWFxgZGiUmJygpKjQ1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoOEhYaH
iImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4eLj5OXm5+jp
6vHy8/T19vf4+fr/xAAfAQADAQEBAQEBAQEBAAAAAAAAAQIDBAUGBwgJCgv/xAC1EQACAQIEBAME
BwUEBAABAncAAQIDEQQFITEGEkFRB2FxEyIygQgUQpGhscEJIzNS8BVictEKFiQ04SXxFxgZGiYn
KCkqNTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqCg4SFhoeIiYqSk5SVlpeY
mZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2dri4+Tl5ufo6ery8/T19vf4+fr/
2gAMAwEAAhEDEQA/AOswzEUy/YrYSqvZTT5p1to1LEZY4HvUV+D9gmPqtfP31Po0jkIT+75P41ft
SdtZkDAxde/JrStgNnWuqYJF8YPPelzwM+tRr65p+CPvcZ9ay1ZWi3BiOh60KRzzU0Vos7jMmFyO
VGatHRixJhn57B14z9R/hWns5GTqwTsZxPWoLhd0Z561ZuIZbSXy7hNhP3T2b6H/AD1qvKwCYBpJ
NMtWkrov6TIZbbY38JxmrzIBWTpbfIxHrWoZBjmoqLURYsro2d2jn7p4b6VR8Uad9kvxfR5MN0ef
Zsf1H9aWSTIwOlbGnmPWNKm0u4OGC/IT1HoR9DUxdmZzXL7xyCDcduetc/rXiJrab7Ppsg81chpx
yF7ED/GmeI9UuLG4m0iI4nQlJpF/hx2H+NcsVCrhc8HBr0KNC/vSInU6RPSL6/N7r0Cqf3Ub4Hua
39SONOlP+xXGWPF9bnHG+uz1H/kGyZ6ba4KkEmkdCZw9oMRDn8a07fpyeKy7Q5i+7xWraI8siRRj
LuwRR05JwK6JiiWWl8oZUnf/AA4PI96bESGAxn29KjuNou51VjsRyqkkdAcVZhVVGG6nsex9aiOg
5aotwZVjkbcd/wClb9q26NQOXYA7QP8APH+NYdpDJeurqdkAOC5HU/7PrXRWoSFNsfA7sev41rHX
c461iw9lFcxGK5RWjb+D/wCv2P0ri/Eelvoc6MrSSWc2BHI+Mhu6kjv3HT9K7lGU+/qae8UF3A9v
cxLLC4w6MMg1q4pnPCrKm79DgtDYPC5680lzqS2urxWcvCTA7SfUVPp+nS6RfXmnyEuI3zG5GN6H
of6fUGuY8Zxzz6vYxWcbyXTE+Wka5YmueMOaq4M9FzXJzI7AgZFZGv8AiF/D9shtH238oIjOM7F6
FsfoPf6V0ukaDfT6ZbvdyxJdFAWRfmXPpuzz+HHua8c1G9n1HVJ7u6UpLuKiM/8ALMDjb+H+NVQw
7cve2RE68GrRepVZnZ3eR2aVyWkcnJJPJ5PU1saH4fbVZUaUFLYEGRl64/uj3NUtL06XVL6O3iJA
J3M/ZV7mvUrCzisrdI402oowqnr9frXZWq8istzCKuc3aBTeQ/79dffL/wASuTP9yuSsI/8ATY/Y
g1198M6ZN/uH+VeVV+JHd2ODtPliBwK29Bw+rWqsBwWcZ9VUkfqBWFa5MQB45rd8NkNr0CYzvRwv
sdpP8ga2qaXJ+yRXEUlvdOGYFmJIPuTVyCNWyzklODgcbu1T6vDFDONw3GOQhs9OlMV8cn75GQD2
qIPmjccjTtpCrYYYA+6p4CgVdEh4JOP0z/hWBLffZYSxYArgln6L/Ln+X882LxNuMu+UZjKhVjXh
jnkEnofTB611U6U5L3TlqWvqdxHcYyq469D6VKLjZklsk8/SuBfxhMZUt7PTneeViqDOQvygkt06
AjPIA55712djDJJCst0oWRlBaPdnafT3+p/IVThKHxGElFk80YvscYZT8r7cnHcfSn2um2lvKZlh
RpmGGkPJI7jPp04HHHep1AJIzxWXe+JrK0c29qGu7nHyrCMrnGRlumPpmpsr8z3JTk1yLY1zqFra
ieaeUKtvG0jnrhQMk49hXzqXknk3yEmSRizHHLEnn+dd74p1p7XSLiwmcDUrzAeOM/6iLO47m7sw
4x6EnvzznhbTjfavG0ifuoR5jcdecKK6KbtFyZUYWeh13hbRhp1j5jg+dPh3z2HZa32zS7SqY700
nmuKcnJ3Z0KyOWgfZew47sBXYXgzpsn+5XFxHF3DnpvFdpdDOmSf7lYVVqjpvsefWnMZ479q0bWS
S3lS6ix5kLB1Bzg47HHY/wBapWfEZOO/NalnGZcRoBkn/Ofat6gR1RveIJ42uLaZMZljEyp6Buh/
Dn/OKwby+j0+HzZGyWzgEnLH8On9MdsU2bV43gd5UffZk2yhhy5XkYGfQgDp2FcddyzTSiebbiR3
VgpGOAoJ44PLcex75rbD0G99kc85qKsixcX730kDNI0kJ+Vi5IIAGCOPUDlvQ1Xs4rpb37GUdpUZ
iqGLcAc43EZ/Dv7dqggLu3mRqS+8p5eAST0VccnJyR07fl6V4Y0BNLtku5wvnygNK5Oeg5OT0HU/
jnvgdtSoqcVY593qSeGNFh0uGW6kjO92yCxBbPHU/UD8snJ5ro4g80mFzgdax5tSt7YMjiQQbyfN
VMg5Oe3IHbJFblhNEYw0Lq6EZBU8GuGpU5pNleyfLdnP+NTNa2MM5ndbXdiWID5W9M45I9qwb/xO
+j6Fbm0iV7i43LHIwGIwMc4xhiM454z69K6vxleWE/h+4s3k3zleI0GWH1x0/GvLn33HhPTXkbJh
uJYQMcqMKQP0NXTSmrS7jtZJoxpWdizyyNJLK255GOSxPJJPrXoXg6x+y2Ku64aYCQ5/8dH5c/jX
EWVr9u1SG2/hZsMf9kcn9M16rBGiWyDbjPOB2qsRK0eUI9yy1QuMDtSPuUjYx+hqJ5yn+sHB7iuV
J9CrnIB9l7bjdk+av869AuBnS3IP8FeWpMw1G1DcEzL/ADr1KYn+zH/3KyxEbWOlO5wVnbs0JEfz
Pg7Vzjcew/GrR1eLSNPlhLo+oO22IRoSqMQPvv3ABzhR7d+ILaQQabLLn+HIG3knOMAn+fP0NUBG
LrVVZp3fDBN8zljkg8DnPvn+WDn0KGHVS7kYV6vLoirK95Lq7/aJQbpsZIIkAyx4XJPA9+f0xQ1j
zItQkidQqSIp+UDrtUcHvyo/KmRtcTaviGXzZJmKb8EBwOny45HAwMelXtStvtEVnaeaZNUc/MMF
sAnPzNjoFwQQSAAfSuuU4xSijmSb1ZpeCtHfUdRfUpyPLizhnXhuMFj+vJBz83ORmus1kzeILM29
pM8Nsv3SBgynPU9wM9B+J5xjndY1KLSrKy8O6a5DyKsl26kfcPITPqRy3T8ckV0+lybVXHTArza8
3zKXc6qNNNN9jLsfCltDBdPJ9o+1F1eGYHb5WOoyDk5BwcjH0ro9E877PdiMr56wsY8jjd2zUtzc
ILdm4AxzmmaC5bUpI9uPNjIH5Vk588lcvltTkcBrmkSQwvLJLcXF4ZN5nkPAXnI69yR2qhaqr+EJ
htyYpg/XkFg36fKK6rxbcpDpc78EuNoHrmuU05ZG0DXYFH7xVgkXHYAsT+ma66U3KN2Z1IpWsWfB
dmJr2S5YZCLsB9zyf0H613pbmsDwjaLbaKkhXDS5k/Pp+gH51tSZHTpWFWXNNkrRBLMFGTUsWm3N
1h5D5SH1HJp+k2guZzO4zHGcAHu1dGsWcFuvpWLqNPljuKT7ni+ntHPqttkAjzAf1r0+Uf8AEvk4
42V5T4cLSakp3Y2YOPXkV6vKc6a/ulVilaSRvB31PPCwTS5HeTaWddpbpjd3z+P5Z+ubbz5v/MC7
40dn3dGyflLEfTGBjAx65qe/u1kskh8w+Y4d39yoYADHQYC/kaj0VYWtpVeRwd8KZDZwHyzZ46bl
HX6e9ethlaJyV5aj7zTZ9VuMWsO6W6CFdp2rEVwp+i4IAznOeK03S18K3AuYCs2pkGJvMO0FiRhA
Bxgd8dAAMg1b0uZNKkW5uOGEgV5l5Ix8wKj04/OuP1fU5NXvWkwViQ4iTPIGep9WPUnufYAVFVNz
a6Ch8I21lebV47y5lMssxcu56scf/Xx6V6PZMBGpB4IyK8xt5URlJGdrZx+Fddouro/l28j7ZAuM
HvXFiot2aO3DySujrGuYHRoXZd2PmUnpmpLT+z7UG6a7Am+YKPN4z2x69/yqssP2raylc+tR61dn
SNKkmkMZK4ChYwPmJwCfXr+hrmgm2aTtbc4vxjqH2nVI7IMdkOGcdPmP+A/nUeioh1HULUf6u5s3
T3yWXH6Viwh57lZJmZpJZeSe5J5/nXSaBH/xUVl8uUYyKSf+uf8A9jXoSXJHlXQ5L812dnBElvaR
woMIihVA7ADFMdjg1MSAB0qrcvsRvpXHFNhJnT6JCBp8TevzH6mtRVyKg0pNuk2YYYJiUkfhU0sq
RKWkkVFAySTgCsFo22ZSfM9DxCTSZdCu4ruJ98LuoH4kV6afm085B5j6A9a8/jme4+HMUzHc6Sou
T1wHH9K9CyP7NH/XOujGaNHRQldHlGrzzf219jnKxRQqQ2xPlQEA/KAM4wFIznJyc/Nmm6fKkO+3
YqyXSqsyqwyrg/KVzjPJI9B16daGqMTrF78iqzysMAYAAPH0PT/JNF0FiMCr8yxRKQccOSST+eeO
h47GvWhokcktTrrG6+1gx28jCIbQCxyXx1Jzwc5PbHPI61zt9BHa3QaKMxRXEqlYj1UA/wAv5EMP
4TV7RJ3kniCoEdm3EKSNyZIJz9R9efrUmsETxy28Dx3ck98twwjI8xQildu0c/xHjqMfU1VTVXJh
ozmo1YEHqQcYGa6fw/ao8ky3EKSbCCN4zgEZ6GsKG2ee+ECKULyglSOUwa7Gwt9uuXDdQsSLnHcZ
/oRXBiJpKx20Yu9zdtVEKjYiJ7BQK1P7NsdTtDBqECTKxBweD+Y5rOQ7SK07WTkc1wRk73RrVjoc
f4g8AS6Wo1HSJGntYjvlgcZdB6g/xD24I96y9ALXGrxBDho43kJ/DZ/7MK9js34BJ715w+i/2P48
1NIgqWjQCSFRjhZG3EYHYMjAD0xXZ7Ryg+Y44OzsTSQSxA+TKRz061UeW8m3J5as3GAP4iTjFacr
YBIrU8N6bHdb7ubJ2yBUX3HOaxc+WNzTTqbeqalFo2lSXlxny4k4Vep9APevJtXur7xBeG5vi4tw
cJbhjtX0z2JrsvG94bi4hsU+4jbnx/e7VycjqhOPuJwo9T3qaCtqtyowsrsZodvK3gO/ikRlMUhb
a3pwa7+Nt2khjwPKyT+Fcp4UL6voOpQjaCiBWGepIP8AhXTptHh1y4yPs5yM44x71WMu2iaDtc8h
1Vx/bV1tRUHmkcj7uev86rSYEQZUHAwdx7YpzyG7vJ2flmcsD+NT28UBZUlO1HLDJ4wpKgNjnPf8
q9WK0MHuaGkDzFaW2kUXUfzlJF+/GeDjHXB5zx1PHrQ1WcCdAVxcxDa8h6ycDBx39jwcH2qxAIrW
4j81VOEDsOnQ7G9uGG7OR1PfFVX02e41V4gM/PnzGO4EeuR1p1JJR1FCN5aGt4ei8tJNSvH2oigB
nJ6Cuo0VHeKS7mQq9w2/aeoGMKD74xWfaaQpWP7TI0qxnckeMID9B1/HNb8RwMV4tWqpN2PUpwsk
TMMc4qxBIBjmoV5FKikMDnvWCZUldWOjsJcgCsfxsr21vbX9vHH5zP5DORyRgso+md351as5NpFW
Nfh+2+Gb2Nc71j8xdoySV+YD8cY/GuunqrHnTXLM85l8SLbAC8t2X1dORXcaE32aE3jSFLP7P5+G
OMgjgn04rgUto79cOobuRV3V7m8utJhsmcRWkSCMIvHmY9TSqxUrRRrGN02VL7VTql/NNHwjMSGP
YetUbqbybXzFzxwgpIIDKyW0K4XILvjqP8KralOJrhbO1+YqMYFdMKajohSm2f/Z"""),
"jpeg", "RGB", "")

if __name__ == "__main__":

    import Tkinter

    root = Tkinter.Tk()
    root.title("demoImage")

    widget = ImageView(root)
    widget.pack(fill="both", expand=1)

    widget.setimage(image)

    root.mainloop()

